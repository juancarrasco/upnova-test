<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalculoPersona extends Model
{
  public $timestamps = false;
  /**
 * The database table used by the model.
 *
 * @var string
 */
protected $table = 'calculo_persona';

/**
* The database primary key value.
*
* @var string
*/
protected $primaryKey = 'id';

/**
 * Attributes that should be mass-assignable.
 *
 * @var array
 */
protected $fillable = ['id_persona', 'id_tipo_calculo', 'date_start'];


}
