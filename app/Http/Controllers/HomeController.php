<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\CalculoPersona;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $weekMap = [
        0 => 'DOMINGO',
        1 => 'LUNES',
        2 => 'MARTES',
        3 => 'MIERCOLES',
        4 => 'JUEVES',
        5 => 'VIERNES',
        6 => 'SABADO',
    ];
      $calculo = CalculoPersona::where('calculo_persona.id_persona', '=', Auth::user()->id)
          ->select('*')
          ->join('persona', 'persona.id', '=', 'calculo_persona.id_persona','left')
          ->get();
      $edad = Carbon::parse($calculo[0]->fecha_nacimiento)->age;
      $dia = Carbon::parse($calculo[0]->fecha_nacimiento)->dayOfWeek;
$weekday = $weekMap[$dia];

          return view('home', compact('calculo','weekday','edad'));
    }


    public function logout(){

        try {
          Auth::logout();
        }
        catch (\Exception $e) {
            //return $e->getMessage();
        }

        return redirect('/');
    }
}
