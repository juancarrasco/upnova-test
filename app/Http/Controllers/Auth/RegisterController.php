<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\TipoCalculo;
use App\CalculoPersona;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|string|max:255',
            'fecha_nacimiento' => 'required|date|before:today',
            'rut' => 'required|unique:persona',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      $input  = $data['fecha_nacimiento'];
      $format = 'YYYY-MM-DD';
      try {

        $date = date("Y-m-d", strtotime($input));
        $user =  User::create([
            'nombre' => $data['nombre'],
            'fecha_nacimiento' => $date,
            'rut' => $data['rut'],
            'date_start' => Carbon::now()->toDateTimeString(),
            //'password' => Hash::make($data['password']),
        ]);
      } catch (Exception $e) {
        dd($e);
      }

        $user =  User::create([
            'nombre' => $data['nombre'],
            'fecha_nacimiento' => $date,
            'rut' => $data['rut'],
            'date_start' => Carbon::now()->toDateTimeString(),
            //'password' => Hash::make($data['password']),
        ]);

        CalculoPersona::create([
          'id_persona' =>  $user->id,
          'id_tipo_calculo' => $data['calculo'],
          'date_start' => Carbon::now()->toDateTimeString()
        ]);



        return $user;
    }

    public function showRegistrationForm(){
      $calculo = TipoCalculo::all();
      return view('auth.register',compact('calculo'));
  }
}
