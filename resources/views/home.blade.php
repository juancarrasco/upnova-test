@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Bienvenido</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if ($calculo[0]->id_tipo_calculo === '1')
                    <div class="card" style="width: 18rem;">
                      <div class="card-body">
                      <h5 class="card-title">Tu edad es:</h5>
                      <p class="card-text">{{$edad}}</p>
                      </div>
                    </div>
                    @else
                    <div class="card" style="width: 18rem;">
                      <div class="card-body">
                      <h5 class="card-title">Naciste un dia:</h5>
                      <p class="card-text">{{$weekday}}</p>
                      </div>
                    </div>

                    @endif
                    Programado por juan andres carrasco!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
